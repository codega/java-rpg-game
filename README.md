# Java Task 3 - RPG

This is a WIP-task for a collaboration RPG-project.


In its current state, the program can create and add Characters to a party, change the party members equipment, perform attacks and spells. The members can also be attacked. 


When the project is finished, it will consist of several tower-levels, with enemies of different difficulty spawning on each level. 



### Author

Charlotte Ødegården