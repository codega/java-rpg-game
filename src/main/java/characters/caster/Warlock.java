package main.java.characters.caster;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.*;
import main.java.characters.abstractions.*;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.*;
import main.java.spells.abstractions.*;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/

public class Warlock extends Character implements CasterCharacter {
    //Equipment
    private Armor armor;
    private MagicWeapon weapon;
    private DamagingSpell damagingSpell;

    //Status tracker
    private double currentMaxHealth;

    //Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED;
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES;
    private final double baseMagicPower = CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER;

    // Constructor
    public Warlock() {
        this.currentMaxHealth = baseHealth;
    }

    //Equips a spell, only if it is a DamagingSpell
    public void equipSpell(Spell spell){
        if (spell instanceof DamagingSpell){
            this.damagingSpell =  (DamagingSpell) spell;
        } else {
            System.out.println("Not a damaging spell!");
        }
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    //Equips new armor, only if it's Cloth-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Cloth){
            this.armor = armor;
            this.currentMaxHealth = baseHealth + armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Magic-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    public String getName(){ return "Warlock";}

    //Calculates and returns the amount of attack points the character does.
    public double castDamagingSpell() {
        return baseMagicPower * weapon.getMagicPowerModifier() * damagingSpell.getSpellDamageModifier();
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
