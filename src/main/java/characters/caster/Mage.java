package main.java.characters.caster;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.*;
import main.java.characters.abstractions.*;
import main.java.characters.abstractions.Character;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.*;
import main.java.spells.abstractions.*;

/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/

public class Mage extends Character implements CasterCharacter {
    //Equipment
    public Armor armor;
    public MagicWeapon weapon;
    private DamagingSpell damagingSpell;

    //Status tracker. Deleted isDead() bc YAGNI
    private double currentMaxHealth;

    //Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.MAGE_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED;
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES;
    private final double baseMagicPower = CharacterBaseStatsOffensive.MAGE_MAGIC_POWER;

    // Constructor
    public Mage() {
        this.currentMaxHealth = baseHealth;
    }

    //Equips a spell, only if it is a DamagingSpell
    public void equipSpell(Spell spell){
        if (spell instanceof DamagingSpell){
            this.damagingSpell =  (DamagingSpell) spell;
        } else {
            System.out.println("Not a damaging spell!");
        }
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    public String getName(){ return "Mage";}

    //Equips new armor, only if it's Cloth-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Cloth){
            this.armor = armor;
            this.currentMaxHealth = baseHealth * armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Magic-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    //Calculates and returns the amount of attack points the character does.
    public double castDamagingSpell() {
        return baseMagicPower * weapon.getMagicPowerModifier() * damagingSpell.getSpellDamageModifier();
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
