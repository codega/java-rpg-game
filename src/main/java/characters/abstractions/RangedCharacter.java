package main.java.characters.abstractions;

public interface RangedCharacter {
    public double attack();
}
