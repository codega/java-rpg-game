package main.java.characters.abstractions;

import main.java.spells.abstractions.Spell;

public interface HealerSupportCharacter {
    public double healPartyMember();
    public void equipSpell(Spell spell);
}
