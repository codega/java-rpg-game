package main.java.characters.abstractions;

import main.java.spells.abstractions.Spell;

public interface CasterCharacter  {
    public void equipSpell(Spell spell);
    public double castDamagingSpell();
}
