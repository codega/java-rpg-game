package main.java.characters.abstractions;

import main.java.spells.abstractions.Spell;

public interface ShieldSupportCharacter  {
    public double shieldPartyMember(double partyMemberMaxHealth);
    public void equipSpell(Spell spell);
}
