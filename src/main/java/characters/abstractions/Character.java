package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;

public abstract class Character {
    //these are methods that ALL characters need to have
    public abstract void equipArmor(Armor armor);
    public abstract void equipWeapon(Weapon weapon);
    public abstract double takeDamage(double incomingDamage, String damageType);
    public abstract String getName();
    public abstract double getCurrentMaxHealth();
}
