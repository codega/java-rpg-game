package main.java.characters.healersupport;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.HealerSupportCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.*;
import main.java.spells.abstractions.*;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/

public class Druid extends Character implements HealerSupportCharacter {
    // Equipment
    private Armor armor;
    private MagicWeapon weapon;
    private HealingSpell healingSpell;

    //Status tracker
    private double currentMaxHealth;

    // Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.DRUID_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED;
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES;

    // Constructor
    public Druid() {
        this.currentMaxHealth = baseHealth;
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    public String getName(){ return "Druid";}

    //Equips new armor, only if it's Leather-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Leather){
            this.armor = armor;
            this.currentMaxHealth = baseHealth + armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Magic-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    //Equips a spell, only if it is a HealingSpell
    public void equipSpell(Spell spell){
        if (spell instanceof HealingSpell){
            this.healingSpell =  (HealingSpell) spell;
        } else {
            System.out.println("Not a healing spell!");
        }
    }

    //Calculates and returns the amount of Health Points the Druid can heal.
    public double healPartyMember() {
        return healingSpell.getHealingAmount() * weapon.getMagicPowerModifier() * weapon.getItemRarity();
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
