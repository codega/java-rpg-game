package main.java.characters.melee;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.MeleeCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.Weapon;

/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends Character implements MeleeCharacter {
    //Equipment
    private BladeWeapon weapon;
    private Armor armor;

    //Status tracker
    private double currentMaxHealth;

    //Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED; // Armor
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES; // Magic armor
    private final double baseAttackPower = CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER;

    // Constructor
    public Warrior() {
        this.currentMaxHealth = baseHealth;
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }  // Needs alteration after equipment is in

    public String getName(){ return "Warrior";}

    //Equips new armor, only if it's Plate-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Plate){
            this.armor = armor;
            this.currentMaxHealth = baseHealth + armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Blade-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof BladeWeapon){
            this.weapon = (BladeWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    //Calculates and returns how many Attack Points the Warrior does when attacking.
    public double attack() {
        return baseAttackPower * weapon.getAttackWeaponModifier() * weapon.getItemRarity();
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
