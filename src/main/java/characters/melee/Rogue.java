package main.java.characters.melee;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.*;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.MeleeCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Leather;
import main.java.items.weapons.abstractions.*;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/

public class Rogue extends Character implements MeleeCharacter {
    //Equipment
    private BladeWeapon weapon;
    private Armor armor;

    //Status tracker
    private double currentMaxHealth;

    //Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED;
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES;
    private final double baseAttackPower = CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER;

    // Constructor
    public Rogue() {
        this.currentMaxHealth = baseHealth;
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    public String getName(){ return "Rogue";}

    //Equips new armor, only if it's Leather-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Leather){
            this.armor = armor;
            this.currentMaxHealth = baseHealth + armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Blade-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof BladeWeapon){
            this.weapon = (BladeWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    //Calculates and returns how many Attack Points the Rogue does when attacking.
    public double attack() {
        return baseAttackPower * weapon.getAttackWeaponModifier() * weapon.getItemRarity();
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
