package main.java.characters.melee;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.*;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.MeleeCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Plate;
import main.java.items.weapons.abstractions.*;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/

public class Paladin extends Character implements MeleeCharacter {
    //Equipment
    private BluntWeapon weapon;
    private Armor armor;

    //Status tracker
    private double currentMaxHealth;

    //Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED;
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES;
    private final double baseAttackPower = CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER;

    // Constructor
    public Paladin() {
        this.currentMaxHealth = baseHealth;
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    public String getName(){ return "Paladin";}

    //Equips new armor, only if it's Plate-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Plate){
            this.armor = armor;
            this.currentMaxHealth = baseHealth + armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Blunt-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof BluntWeapon){
            this.weapon = (BluntWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    //Calculates and returns how many Attack Points the Paladin does when attacking.
    public double attack() {
        return baseAttackPower * weapon.getAttackWeaponModifier() * weapon.getItemRarity();
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
