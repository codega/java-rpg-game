package main.java.characters.ranged;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.*;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.RangedCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Mail;
import main.java.items.weapons.abstractions.*;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/

public class Ranger extends Character implements RangedCharacter {
    //Equipment
    private Armor armor;
    private RangedWeapon weapon;

    //Status tracker
    private double currentMaxHealth;

    //Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.RANGER_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED;
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES;
    private final double baseAttackPower = CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER;

    // Constructor
    public Ranger() {
        this.currentMaxHealth = baseHealth;
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    public String getName(){ return "Ranger";}

    //Equips new armor, only if it's Mail-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Mail){
            this.armor = armor;
            this.currentMaxHealth = baseHealth + armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Range-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof RangedWeapon ){
            this.weapon = (RangedWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    //Calculates and returns how many Attack Points the Ranger does when attacking.
    public double attack() {
        return baseAttackPower * weapon.getAttackWeaponModifier() * weapon.getItemRarity();
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
