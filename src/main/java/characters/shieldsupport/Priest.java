package main.java.characters.shieldsupport;
// Imports
import com.sun.jdi.InvalidTypeException;
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.ShieldSupportCharacter;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.weapons.abstractions.*;
import main.java.spells.abstractions.*;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/

public class Priest extends Character implements ShieldSupportCharacter {
    //Equipment
    private Armor armor;
    private MagicWeapon weapon;
    private ShieldingSpell shieldingSpell;

    //Status tracker
    private double currentMaxHealth;

    //Base stats
    private final double baseHealth = CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH;
    private final double basePhysReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED;
    private final double baseMagicReductionPercent = CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES;

    // Constructor
    public Priest() {
        this.currentMaxHealth = baseHealth;
    }

    public double getCurrentMaxHealth() {
        return currentMaxHealth;
    }

    public String getName(){ return "Priest";}

    //Equips new armor, only if it's Cloth-type. Updates currentMaxHealth based on armor.
    public void equipArmor(Armor armor) {
        if (armor instanceof Cloth){
            this.armor = armor;
            this.currentMaxHealth = baseHealth + armor.getHealthModifier();
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e) {
                System.out.println("Wrong type of armor!");
            }
        }
    }

    //Equips new weapon, only if it's Magic-type.
    public void equipWeapon(Weapon weapon) {
        if (weapon instanceof MagicWeapon){
            this.weapon = (MagicWeapon) weapon;
        } else {
            try {
                throw new InvalidTypeException();
            } catch (InvalidTypeException e){
                System.out.println("Wrong type of weapon!");
            }
        }
    }

    //Equips a spell, only if it is a ShieldingSpell
    public void equipSpell(Spell spell){
        if (spell instanceof ShieldingSpell){
            this.shieldingSpell = (ShieldingSpell) spell;
        } else {
            System.out.println("Not a shielding spell!");
        }
    }

    //Calculates and returns the amount of Attack Points the Priest can shield a character from, based on that characters health.
    public double shieldPartyMember(double partyMemberMaxHealth) {
        return (partyMemberMaxHealth * shieldingSpell.getAbsorbShieldPercentage()) + (weapon.getMagicPowerModifier() * weapon.getItemRarity());
    }

    //Calculates and returns the amount of attack points given to the character.
    public double takeDamage(double incomingDamage, String damageType) {
        double damage;
        //Damage calculation is based on what type of damage is given.
        if (damageType.equalsIgnoreCase("magic")){
            damage = incomingDamage * (1 - (baseMagicReductionPercent * armor.getMagicRedModifier() * armor.getRarityModifier()));
        } else if (damageType.equalsIgnoreCase("physical")){
            damage = incomingDamage * (1 - (basePhysReductionPercent * armor.getPhysRedModifier() * armor.getRarityModifier()));
        } else {
            return 0;
        }
        //Can't have very little or negative damage.
        if (damage < 1){
            damage = 1;
        }
        return damage;
    }
}
