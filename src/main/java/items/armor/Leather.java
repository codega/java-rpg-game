package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

public class Leather implements Armor {
    private final double rarityModifier;

    public Leather(double itemRarity) {
        this.rarityModifier = itemRarity;
    }

    @Override
    public String getArmorName() {
        return "Leather";
    }

    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

}
