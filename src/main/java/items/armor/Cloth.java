package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

public class Cloth implements Armor {
    private final double rarityModifier;

    public Cloth(double itemRarity) {
        this.rarityModifier = itemRarity;
    }

    @Override
    public String getArmorName() {
        return "Cloth";
    }

    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

}
