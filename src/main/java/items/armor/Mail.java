package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

public class Mail implements Armor {
    private final double rarityModifier;

    public Mail(double itemRarity) {
        this.rarityModifier = itemRarity;
    }

    @Override
    public String getArmorName() {
        return "Mail";
    }

    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

}
