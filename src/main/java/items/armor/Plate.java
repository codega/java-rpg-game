package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;

public class Plate implements Armor {
    private final double rarityModifier;

    public Plate(double itemRarity) {
        this.rarityModifier = itemRarity;
    }

    @Override
    public String getArmorName() {
        return "Plate";
    }

    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.PLATE_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

}
