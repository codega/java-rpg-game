package main.java.items.armor.abstractions;

public interface Armor {
    String getArmorName();
    double getHealthModifier();
    double getPhysRedModifier();
    double getMagicRedModifier();
    double getRarityModifier();
}
