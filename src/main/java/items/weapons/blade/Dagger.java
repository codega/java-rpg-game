package main.java.items.weapons.blade;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Dagger implements BladeWeapon {
    private double itemRarity;
    public Dagger(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Dagger";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}