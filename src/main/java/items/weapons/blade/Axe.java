package main.java.items.weapons.blade;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Axe implements BladeWeapon {
    private double itemRarity;
    public Axe(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.AXE_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Axe";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}