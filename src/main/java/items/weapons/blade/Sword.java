package main.java.items.weapons.blade;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Sword implements BladeWeapon {
    private double itemRarity;
    public Sword(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Sword";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}