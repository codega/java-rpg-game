package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Staff implements MagicWeapon {
    private double itemRarity;
    public Staff(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getMagicPowerModifier() {
        return WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Staff";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}