package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Wand implements MagicWeapon {
    private double itemRarity;
    public Wand(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getMagicPowerModifier() {
        return WeaponStatsModifiers.WAND_MAGIC_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Wand";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}