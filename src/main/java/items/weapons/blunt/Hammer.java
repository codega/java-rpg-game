package main.java.items.weapons.blunt;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Hammer implements BluntWeapon {
    private double itemRarity;
    public Hammer(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Hammer";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}