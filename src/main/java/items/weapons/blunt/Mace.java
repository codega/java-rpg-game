package main.java.items.weapons.blunt;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Mace implements BluntWeapon {
    private double itemRarity;
    public Mace(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.MACE_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Mace";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}