package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Bow implements RangedWeapon {
    private double itemRarity;
    public Bow(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.BOW_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Bow";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}