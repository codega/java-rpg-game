package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Crossbow implements RangedWeapon {
    private double itemRarity;
    public Crossbow(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Crossbow";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}