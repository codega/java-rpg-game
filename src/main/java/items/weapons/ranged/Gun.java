package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Gun implements RangedWeapon {
    private double itemRarity;
    public Gun(double itemRarityModifier){
        this.itemRarity = itemRarityModifier;
    }

    @Override
    public double getAttackWeaponModifier() {
        return WeaponStatsModifiers.GUN_ATTACK_MOD;
    }

    @Override
    public String getWeaponName() {
        return "Gun";
    }

    @Override
    public double getItemRarity() {
        return this.itemRarity;
    }
}