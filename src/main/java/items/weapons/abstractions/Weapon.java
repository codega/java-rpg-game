package main.java.items.weapons.abstractions;

public interface Weapon {
    String getWeaponName();
    double getItemRarity();
}
