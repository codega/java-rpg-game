package main.java.items.weapons.abstractions;

public interface RangedWeapon extends Weapon{
    double getAttackWeaponModifier();
}
