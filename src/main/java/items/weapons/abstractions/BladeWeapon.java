package main.java.items.weapons.abstractions;

public interface BladeWeapon extends Weapon{
    double getAttackWeaponModifier();
}
