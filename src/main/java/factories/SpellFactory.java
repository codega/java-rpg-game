package main.java.factories;

import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.*;
import main.java.spells.healing.*;
import main.java.spells.shielding.*;

public class SpellFactory {
    public Spell getSpell(SpellType spellType) {
        switch(spellType) {
            case ArcaneMissile:
                return new ArcaneMissile();
            case Barrier:
                return new Barrier();
            case ChaosBolt:
                return new ChaosBolt();
            case Regrowth:
                return new Regrowth();
            case Rapture:
                return new Rapture();
            case SwiftMend:
                return new SwiftMend();
            default:
                return null;
        }
    }
}
