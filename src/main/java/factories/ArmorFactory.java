package main.java.factories;
// Imports
import main.java.items.armor.*;
import main.java.items.armor.abstractions.*;

public class ArmorFactory {
    public Armor getArmor(ArmorType armorType, double itemRarityModifier){
        switch(armorType) {
            case Cloth:
                return new Cloth(itemRarityModifier);
            case Leather:
                return new Leather(itemRarityModifier);
            case Mail:
                return new Mail(itemRarityModifier);
            case Plate:
                return new Plate(itemRarityModifier);
            default:
                return null;
        }
    }
}
