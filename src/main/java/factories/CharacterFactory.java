package main.java.factories;
// Imports
import main.java.characters.abstractions.Character;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.caster.*;
import main.java.characters.healersupport.*;
import main.java.characters.melee.*;
import main.java.characters.ranged.*;
import main.java.characters.shieldsupport.*;

public class CharacterFactory {
    public Character getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Mage:
                return new Mage();
            case Warlock:
                return new Warlock();
            case Druid:
                return new Druid();
            case Paladin:
                return new Paladin();
            case Rogue:
                return new Rogue();
            case Warrior:
                return new Warrior();
            case Ranger:
                return new Ranger();
            case Priest:
                return new Priest();
            default:
                return null;
        }
    }
}
