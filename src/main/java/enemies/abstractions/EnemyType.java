package main.java.enemies.abstractions;

public enum EnemyType {
    Beast,
    Dragon,
    Elemental,
    Fiend,
    Giant,
    Humanoid,
    Orc,
    Undead
}
