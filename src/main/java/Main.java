package main.java;

//Imports
import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.Character;
import main.java.characters.caster.*;
import main.java.characters.healersupport.*;
import main.java.characters.melee.*;
import main.java.characters.ranged.*;
import main.java.characters.shieldsupport.*;
import main.java.consolehelpers.*;
import main.java.factories.*;
import main.java.items.armor.abstractions.*;
import main.java.items.weapons.abstractions.*;
import main.java.spells.abstractions.*;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //List of party members
        ArrayList<Character> party = new ArrayList<Character>();

        //Scanner to access user input
        Scanner input = new Scanner(System.in);

        //Creates instance of sub-menu to create characters
        CreateCharactersInteraction create = new CreateCharactersInteraction();

        //Factories
        WeaponFactory weaponFactory = new WeaponFactory();
        ArmorFactory armorFactory = new ArmorFactory();
        SpellFactory spellFactory = new SpellFactory();

        System.out.println("Welcome to the RPG! What do you want to do?");

        //Main menu
        boolean running = true;
        while (running){

            //Main menu options
            String startMenuOption;
            System.out.println("\n 0 - Create new party member!\n 1 - Equip new weapon for party member!");
            System.out.println(" 2 - Equip new armor for party member!\n 3 - Equip new spell for party member!");
            System.out.println(" 4 - Make party member attack!\n 5 - Make party member use a spell!");
            System.out.println(" 6 - Attack a party member!\n 7 - See all party members!");

            //User input decides on what option happens
            startMenuOption = input.next();
            switch (startMenuOption){
                case "0":
                    //Create and add character to party
                    Character character = create.choseACharacterToCreate();
                    party.add(character);
                    break;
                case "1":
                    //Create and equip weapon
                    if (party.size() == 0){
                        System.out.println("There's no party members!");
                    } else {
                        System.out.println("Choose a party member to equip new weapon for!");

                        //Goes through and lists all party members
                        int index = 0;
                        for (Character partyMember : party){
                            System.out.println(index + ": " + partyMember.getName());
                            index ++;
                        }

                        //Gets a party member
                        int partyChoice = input.nextInt();
                        Character chosenPartyMember = party.get(partyChoice);


                        System.out.println("What kind of weapon should they get?");

                        //Lists all weapon-options
                        index = 0;
                        for (WeaponType weapon : WeaponType.values()){
                            System.out.println(index + ": " + weapon.name());
                            index ++;
                        }

                        //User chooses weapon
                        int weaponChoice = input.nextInt();

                        //Gets weapon-rarity, if user fucks up then chooses Common-rarity
                        System.out.println("How rare is this weapon?");
                        System.out.println(" 0 - Common\n 1 - Uncommon\n 2 - Rare\n 3 - Epic\n 4 - Legendary");
                        double rarity = ItemRarityModifiers.COMMON_RARITY_MODIFIER;
                        int rarityChoice = input.nextInt();
                        if (rarityChoice == 0){ rarity = ItemRarityModifiers.COMMON_RARITY_MODIFIER; }
                        else if (rarityChoice == 1) { rarity = ItemRarityModifiers.UNCOMMON_RARITY_MODIFIER;}
                        else if (rarityChoice == 2) { rarity = ItemRarityModifiers.RARE_RARITY_MODIFIER;}
                        else if (rarityChoice == 3) { rarity = ItemRarityModifiers.EPIC_RARITY_MODIFIER;}
                        else if (rarityChoice == 4) { rarity = ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER;}
                        else { System.out.println("Not a valid rarity! Choosing common rarity for this weapon");}

                        //Creates and equips weapon to the chosen party member
                        Weapon chosenWeapon = weaponFactory.getWeapon(WeaponType.values()[weaponChoice], rarity);
                        System.out.println("Equipping weapon: " + chosenWeapon.getWeaponName());
                        chosenPartyMember.equipWeapon(chosenWeapon);
                        break;
                    }
                    break;

                case "2":
                    //Create and equip armor
                    if (party.size() == 0){
                        System.out.println("There's no party members!");
                    } else {
                        System.out.println("Choose a party member to equip new armor for!");

                        //Goes through and lists all party members
                        int index = 0;
                        for (Character partyMember : party){
                            System.out.println(index + ": " + partyMember.getName());
                            index ++;
                        }

                        //Chooses party member
                        int partyChoice = input.nextInt();
                        Character chosenPartyMember = party.get(partyChoice);


                        System.out.println("What kind of armor should they get?");

                        //Goes through and lists all armor options
                        index = 0;
                        for (ArmorType armor : ArmorType.values()){
                            System.out.println(index + ": " + armor.name());
                            index ++;
                        }

                        //Chooses armor
                        int armorChoice = input.nextInt();

                        //Chooses armor rarity, if nothing is chosen -> Common rarity.
                        System.out.println("How rare is this weapon?");
                        System.out.println(" 0 - Common\n 1 - Uncommon\n 2 - Rare\n 3 - Epic\n 4 - Legendary");
                        double rarity = ItemRarityModifiers.COMMON_RARITY_MODIFIER;
                        int rarityChoice = input.nextInt();
                        if (rarityChoice == 0){ rarity = ItemRarityModifiers.COMMON_RARITY_MODIFIER; }
                        else if (rarityChoice == 1) { rarity = ItemRarityModifiers.UNCOMMON_RARITY_MODIFIER;}
                        else if (rarityChoice == 2) { rarity = ItemRarityModifiers.RARE_RARITY_MODIFIER;}
                        else if (rarityChoice == 3) { rarity = ItemRarityModifiers.EPIC_RARITY_MODIFIER;}
                        else if (rarityChoice == 4) { rarity = ItemRarityModifiers.LEGENDARY_RARITY_MODIFIER;}
                        else { System.out.println("Not a valid rarity! Choosing common rarity for this armor"); }

                        //Creates and equips the chosen Armor to the chosen party member.
                        Armor chosenArmor = armorFactory.getArmor(ArmorType.values()[armorChoice], rarity);
                        System.out.println("Equipping armor: " + chosenArmor.getArmorName());
                        chosenPartyMember.equipArmor(chosenArmor);
                        break;
                    }
                    break;

                case "3":
                    //Equip spell
                    if (party.size() == 0){
                        System.out.println("There's no party members!");
                    } else {
                        System.out.println("Choose a party member to equip new spell for!");

                        //Lists all party members
                        int index = 0;
                        for (Character partyMember : party){
                            System.out.println(index + ": " + partyMember.getName());
                            index ++;
                        }

                        //Users chooses party member
                        int partyChoice = input.nextInt();
                        Character chosenPartyMember = party.get(partyChoice);

                        //Only makes the user pick a spell if the chosen character can USE spells
                        if (chosenPartyMember instanceof Mage || chosenPartyMember instanceof Warlock || chosenPartyMember instanceof Druid || chosenPartyMember instanceof Priest){
                            System.out.println("What kind of spell should they get?");

                            //Lists all spells
                            index = 0;
                            for (SpellType spell : SpellType.values()){
                                System.out.println(index + ": " + spell.name());
                                index ++;
                            }

                            //User chooses spell
                            int spellChoice = input.nextInt();
                            Spell chosenSpell = spellFactory.getSpell(SpellType.values()[spellChoice]);

                            //Tries to equip spell. Casts Character to specific Character-type to equip
                            System.out.println("Equipping spell: " + chosenSpell.getSpellName());
                            if (chosenPartyMember instanceof Mage){
                                Mage partyMember = (Mage) chosenPartyMember;
                                partyMember.equipSpell(chosenSpell);

                            } else if (chosenPartyMember instanceof Warlock){
                                Warlock partyMember = (Warlock) chosenPartyMember;
                                partyMember.equipSpell(chosenSpell);

                            } else if (chosenPartyMember instanceof Druid){
                                Druid partyMember = (Druid) chosenPartyMember;
                                partyMember.equipSpell(chosenSpell);

                            } else if (chosenPartyMember instanceof Priest){
                                Priest partyMember = (Priest) chosenPartyMember;
                                partyMember.equipSpell(chosenSpell);
                            }
                        } else {
                            //Error message for Characters without spells
                            System.out.println("That character doesn't have any spells! Can't equip spells for them!");
                        }
                    }
                    break;

                case "4":
                    //Perform an attack
                    if (party.size() == 0){
                        System.out.println("There's no party members!");
                    } else {
                        System.out.println("Choose a character to do an attack with!");

                        //Lists all party members
                        int index = 0;
                        for (Character partyMembers : party){
                            System.out.println(index + ": " + partyMembers.getName());
                            index++;
                        }

                        //Users chooses a Character
                        int pmChoice = input.nextInt();
                        Character chosenMember = party.get(pmChoice);

                        //Casts to specific CharacterType and performs the attack.
                        if (chosenMember instanceof Paladin){
                            Paladin paladin = (Paladin) chosenMember;
                            System.out.println("Attack damage: " + paladin.attack());

                        } else if (chosenMember instanceof Rogue){
                            Rogue rogue = (Rogue) chosenMember;
                            System.out.println("Attack damage: " + rogue.attack());

                        } else if (chosenMember instanceof Warrior){
                            Warrior warrior = (Warrior) chosenMember;
                            System.out.println("Attack damage: " + warrior.attack());

                        } else if (chosenMember instanceof Ranger){
                            Ranger ranger = (Ranger) chosenMember;
                            System.out.println("Attack damage: " + ranger.attack());

                        } else {
                            //Error message for when the user chooses a non-attacking member
                            System.out.println("That character doesn't do any physical attacks!");
                        }
                    }
                    break;

                case "5":
                    //Use spells
                    if (party.size() == 0){
                        System.out.println("There's no party members!");
                    } else {
                        System.out.println("Choose a party member to use a spell!");

                        //Lists all characters in party
                        int index = 0;
                        for (Character partyMember : party){
                            System.out.println(index + ": " + partyMember.getName());
                            index ++;
                        }

                        //User chooses character
                        int partyChoice = input.nextInt();
                        Character chosenPartyMember = party.get(partyChoice);

                        //Checks if chosen character can use a spell
                        if (chosenPartyMember instanceof Mage || chosenPartyMember instanceof Warlock || chosenPartyMember instanceof Druid || chosenPartyMember instanceof Priest){
                            //Casts Character to specific Magic-Character, so they do the correct spell
                            if (chosenPartyMember instanceof Mage){
                                //Mages cast Damaging spells
                                Mage partyMember = (Mage) chosenPartyMember;
                                double damageDone = partyMember.castDamagingSpell();
                                System.out.println(partyMember.getName() + " did " + damageDone + " attack points worth of damage");

                            } else if (chosenPartyMember instanceof Warlock){
                                //Warlocks cast Damaging spells
                                Warlock partyMember = (Warlock) chosenPartyMember;
                                double damageDone = partyMember.castDamagingSpell();
                                System.out.println(partyMember.getName() + " did " + damageDone + " attack points worth of damage");

                            } else if (chosenPartyMember instanceof Druid){
                                //Druids heal some Health points
                                Druid partyMember = (Druid) chosenPartyMember;
                                double canHeal = partyMember.healPartyMember();
                                System.out.println(partyMember.getName() + " healed " + canHeal + " health points");

                            } else if (chosenPartyMember instanceof Priest){
                                //Priests shield some amount of Attack Points based on another characters maxHealth
                                Priest partyMember = (Priest) chosenPartyMember;
                                System.out.println("What party member should the Priest shield?");

                                //Lists all available characters to shield for
                                index = 0;
                                for (Character partyMembers : party){
                                    System.out.println(index + ": " +partyMembers.getName());
                                    index++;
                                }

                                //User chooses character to shield
                                int shieldedPartyChoice = input.nextInt();
                                Character memberToBeShielded = party.get(shieldedPartyChoice);

                                //Calculates the shielding
                                double shieldAmount = partyMember.shieldPartyMember(memberToBeShielded.getCurrentMaxHealth());
                                System.out.println(partyMember.getName() + " can shield " + shieldAmount + " attack points worth of damage for " + memberToBeShielded.getName());
                            }
                        } else {
                            //Error message for when user chose a non-magic character
                            System.out.println("That character doesn't have any spells!");
                        }
                    }
                    break;

                case "6":
                    //Attack a party member
                    if (party.size() == 0){
                        System.out.println("There's no party members!");
                    } else {
                        System.out.println("Choose a party member to attack!");

                        //Lists all party members
                        int index = 0;
                        for (Character partyMember : party){
                            System.out.println(index + ": " + partyMember.getName());
                            index++;
                        }

                        //User chooses character
                        int pmChoice = input.nextInt();
                        Character toBeAttacked = party.get(pmChoice);

                        //User chooses type of attack MAGIC or PHYSICAL
                        System.out.println("Magic or physical attack?");
                        String attackType = input.next();

                        //Checks that user chose valid attack type
                        if (attackType.equalsIgnoreCase("physical") || attackType.equalsIgnoreCase("magic")){
                            System.out.println("Please input attack points: ");

                            //User inputs amount of Attack Points to attack character with
                            double attackPoints = input.nextInt();

                            //Calculates and prints out damage sent and taken
                            double actualDamageTaken = toBeAttacked.takeDamage(attackPoints, attackType);
                            System.out.println(toBeAttacked.getName() + " got sent " + attackPoints + " attack points.");
                            System.out.println(toBeAttacked.getName() + " took " + actualDamageTaken + " attack points after shielding.");
                        } else {
                            //Error message if invalid attack type
                            System.out.println("That's not a valid attack type!");
                        }
                    }
                    break;

                case "7":
                    //List all party members, if any exists.
                    if (party.size() == 0){
                        System.out.println("There's no party members!");
                    } else {
                        for (Character chara : party){
                            System.out.println(chara.getName());
                        }
                    }
                    break;

                default:
                    System.out.println("Sorry, not a valid option! Try again");
            }
        }
    }
}

