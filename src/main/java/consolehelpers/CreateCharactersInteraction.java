package main.java.consolehelpers;

//Imports
//Characters
import main.java.characters.abstractions.Character;
import main.java.characters.caster.*;
import main.java.characters.healersupport.*;
import main.java.characters.melee.*;
import main.java.characters.ranged.*;
import main.java.characters.shieldsupport.*;

//Factories
import main.java.factories.*;

//Items
import main.java.items.armor.abstractions.*;
import main.java.items.weapons.abstractions.*;
import main.java.characters.abstractions.*;
import main.java.spells.abstractions.*;
import main.java.basestats.ItemRarityModifiers;

//Java packages
import java.util.Scanner;

public class CreateCharactersInteraction {
    final private CharacterFactory characterFactory;
    final private ArmorFactory armorFactory;
    final private WeaponFactory weaponFactory;
    final private SpellFactory spellFactory;
    final private Scanner scanner;

    public CreateCharactersInteraction(){
        this.characterFactory = new CharacterFactory();
        this.armorFactory = new ArmorFactory();
        this.weaponFactory = new WeaponFactory();
        this.spellFactory = new SpellFactory();
        this.scanner = new Scanner(System.in);
    }

    //Creates and returns a character of the users choice.
    public Character choseACharacterToCreate(){
        boolean run = true;
        //Makes a sub menu
        while (run){
            System.out.println("Choose a character to create and add to party!");

            //Lists all character types available
            int count = 0;
            for (CharacterType character : CharacterType.values()){
                System.out.println(count + ": " + character.name());
                count++;
            }

            //Gets the users choice, creates that specific type of Character.
            String characterChoice = scanner.nextLine();
            switch (characterChoice){
                case "0":
                    return createDruid();
                case "1":
                    return createMage();
                case "2":
                    return createPaladin();
                case "3":
                    return createPriest();
                case "4":
                    return createRanger();
                case "5":
                    return createRogue();
                case "6":
                    return createWarlock();
                case "7":
                    return createWarrior();
                default:
                    System.out.println("Not a valid character!");
            }
        }
        return null;
    }

    //Creates a default Mage with Common Staff, Common Cloth and ChaosBolt-spell
    public Mage createMage(){
        Mage mage = (Mage) characterFactory.getCharacter(CharacterType.Mage);
        DamagingSpell spell = (DamagingSpell) spellFactory.getSpell(SpellType.ChaosBolt);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Staff, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Armor armor = armorFactory.getArmor(ArmorType.Cloth, ItemRarityModifiers.COMMON_RARITY_MODIFIER);

        mage.equipSpell(spell);
        mage.equipWeapon(weapon);
        mage.equipArmor(armor);
        return mage;
    }

    //Creates a default Warlock with Common Wand, Common Cloth and ArcaneMissile-spell
    public Warlock createWarlock(){
        Warlock warlock = (Warlock) characterFactory.getCharacter(CharacterType.Warlock);
        DamagingSpell spell = (DamagingSpell) spellFactory.getSpell(SpellType.ArcaneMissile);
        Armor armor = armorFactory.getArmor(ArmorType.Cloth, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Wand, ItemRarityModifiers.COMMON_RARITY_MODIFIER);

        warlock.equipArmor(armor);
        warlock.equipWeapon(weapon);
        warlock.equipSpell(spell);

        return warlock;
    }

    //Creates a default Druid with Common Staff, Common Leather and Regrowth-spell
    public Druid createDruid(){
        Druid druid =  (Druid) characterFactory.getCharacter(CharacterType.Druid);
        Armor armor = armorFactory.getArmor(ArmorType.Leather, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Staff, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        HealingSpell spell = (HealingSpell) spellFactory.getSpell(SpellType.Regrowth);

        druid.equipArmor(armor);
        druid.equipSpell(spell);
        druid.equipWeapon(weapon);

        return druid;
    }

    //Creates a default Paladin with Common Hammer and Common Plate
    public Paladin createPaladin(){
        Paladin paladin = (Paladin) characterFactory.getCharacter(CharacterType.Paladin);
        Armor armor = armorFactory.getArmor(ArmorType.Plate ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Hammer ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);

        paladin.equipArmor(armor);
        paladin.equipWeapon(weapon);

        return paladin;
    }

    //Creates a default Rogue with Common Dagger and Common Leather
    public Rogue createRogue(){
        Rogue rogue = (Rogue) characterFactory.getCharacter(CharacterType.Rogue);
        Armor armor = armorFactory.getArmor(ArmorType.Leather ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Dagger ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);

        rogue.equipArmor(armor);
        rogue.equipWeapon(weapon);
        return rogue;
    }

    //Creates a default Warrior with Common Sword and Common Plate
    public Warrior createWarrior(){
        Warrior warrior = (Warrior) characterFactory.getCharacter(CharacterType.Warrior);
        Armor armor = armorFactory.getArmor(ArmorType.Plate ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Sword ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);

        warrior.equipArmor(armor);
        warrior.equipWeapon(weapon);
        return warrior;
    }

    //Creates a default Warrior with Common Gun and Common Mail
    public Ranger createRanger(){
        Ranger ranger = (Ranger) characterFactory.getCharacter(CharacterType.Ranger);
        Armor armor = armorFactory.getArmor(ArmorType.Mail ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Gun ,ItemRarityModifiers.COMMON_RARITY_MODIFIER);

        ranger.equipArmor(armor);
        ranger.equipWeapon(weapon);
        return ranger;
    }

    //Creates a default Priest with Common Staff, Common Cloth and Barrier-spell
    public Priest createPriest(){
        Priest priest = (Priest) characterFactory.getCharacter(CharacterType.Priest);
        ShieldingSpell spell = (ShieldingSpell) spellFactory.getSpell(SpellType.Barrier);
        Armor armor = armorFactory.getArmor(ArmorType.Cloth, ItemRarityModifiers.COMMON_RARITY_MODIFIER);
        Weapon weapon = weaponFactory.getWeapon(WeaponType.Staff, ItemRarityModifiers.COMMON_RARITY_MODIFIER);

        priest.equipArmor(armor);
        priest.equipWeapon(weapon);
        priest.equipSpell(spell);

        return priest;
    }
}
